Rails.application.routes.draw do
    get '/images/index', to: 'images#index'
    root 'images#index'
    get 'images/new', to: 'images#new'
    post 'images', to: 'images#create'
    delete 'images/:id', to: 'images#destroy'
    get 'images/:id/edit', to: 'images#edit'
    patch'images/:id',to:'images#update'
end
